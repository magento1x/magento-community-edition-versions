# Magento CE Versions!
Archive of [https://magento.com/tech-resources/download](https://magento.com/tech-resources/download)

Note: all but the first download are older versions of the software, and are not the latest release available. The older version does not include latest security patches. Please make sure to apply the latest security patches to those version.

# XMLConnect Module
This is the latest stable release of the XMLConnect Module.
-ver 24  - Added Apr 22, 2015
[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/XMLConnectV24-2015-04-22-05-26-31.zip)

# Database Repair Tool
The Database Repair Tool compares 2 databases (reference and target), and updates the target database so it has the same structure as the reference database. [Learn More »](http://www.magentocommerce.com/wiki/doc/db-repair-tool)
- ver 1.2- Added September 19, 2014
[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-db-repair-tool-1.2-2015-02-09-10-34-22.zip)


# All 1.9.x CE Versions

## ver 1.9.4.3  - Added Oct 8, 2019 

- Includes patch SUPEE-11219 as well as all previous security patches

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.4.3-2019-10-08-05-29-43.zip)

## ver 1.9.4.2  - Added Jun 25, 2019 

- Includes patch SUPEE-11155 as well as all previous security patches

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.4.2-2019-06-19-04-33-08.zip)

## ver 1.9.4.1  - Added Mar 26, 2019

- Includes patch SUPEE-11086 as well as all previous security patches and PHP 7.2 compatibility patch - Includes dashboard charts patch MPERF-10509.diff - Does not include Authorize.net Signature Key patch due to issues with signature generation for non-English characters in addresses

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.4.1-2019-03-22-09-07-15.zip)

## ver 1.9.4.0  - Added Nov 28, 2018

- Includes patch SUPEE-10975 as well as all previous security patches and PHP 7.2 compatibility patch

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.4.0-2018-11-28-04-28-56.zip)

## ver 1.9.3.10  - Added Sep 18, 2018

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652, SUPEE-8167, SUPEE-9767v2, SUPEE-10266, SUPEE-10415, SUPEE-10570, SUPEE-10752, SUPEE-10888

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.10-2018-09-18-03-18-14.zip)

## ver 1.9.3.9 - Added Jun 27, 2018

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652, SUPEE-8167, SUPEE-9767v2, SUPEE-10266, SUPEE-10415, SUPEE-10570, SUPEE-10752

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.9-2018-06-27-02-40-21.zip)

## ver 1.9.3.8 - Added Feb 27, 2018

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652, SUPEE-8167, SUPEE-9767v2, SUPEE-10266, SUPEE-10415, SUPEE-10570  
- Note: this release does not include SUPEE-10570v2 that resolves a checkout problem. Please make sure to install SUPEE-10570v2 for this version

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.8-2018-02-23-05-50-58.zip)

## ver 1.9.3.8 - Added Feb 27, 2018

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652, SUPEE-8167, SUPEE-9767v2, SUPEE-10266, SUPEE-10415, SUPEE-10570  

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.9-2018-06-27-02-40-21.zip)

## ver 1.9.3.7 - Added Nov 28, 2017

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652, SUPEE-8167, SUPEE-9767v2, SUPEE-10266, SUPEE-10415

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.7-2017-11-27-05-29-14.zip)

## ver 1.9.3.6 - Added Sep 14, 2017

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652, SUPEE-8167, SUPEE-9767v2, SUPEE-10266
 
[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.6-2017-09-14-06-03-39.zip)

## ver 1.9.3.4 - Added Jul 12, 2017

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652, SUPEE-8167, SUPEE-9767v2  
- Resolves known issues introduced in 1.9.3.3

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.4-2017-07-12-04-02-26.zip)

## ver 1.9.3.3 - Deprecated - contains known issues, please use version 1.9.3.4 instead - Added May 31, 2017

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652, SUPEE-8167, SUPEE-9767  
- There are known issues in this release. Please check [here](https://magento.com/security/patches/supee-9767) for details.

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.3-2017-05-31-05-56-29.zip)

## ver 1.9.3.2 - Added Feb 7, 2017

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788, SUPEE-9652

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.2-2017-02-07-01-49-34.zip)

## ver 1.9.3.1 - Added Nov 14, 2016

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.1-2016-11-14-06-05-09.zip)

## ver 1.9.3.0 - Added Oct 11, 2016

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1, SUPEE-8788

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.3.0-2016-10-11-06-06-51.zip)

## ver 1.9.2.4 - Added Feb 23, 2016

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405, SUPEE-7405 v1.1

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.2.4-2016-02-23-06-03-22.zip)

## ver 1.9.2.3 - Added Jan 20, 2016

- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482, SUPEE-6788, SUPEE-7616, SUPEE-7405

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.2.3-2016-01-20-02-52-21.zip)

## ver 1.9.2.1 - Added Aug 4, 2015
- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6237, SUPEE-6285, SUPEE-6482

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.2.1-2015-08-03-06-33-36.zip)

## ver 1.9.2.0 - Added Jul 7, 2015
- Includes patches: SUPEE-5344, SUPEE-5994, SUPEE-6285

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.2.0-2015-07-08-02-48-06.zip)

## ver 1.9.1.1 - Added May 1, 2015
- Includes patch for the SUPEE-5344 issue

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.1.1-2015-04-30-12-49-08.zip)

## ver 1.9.1.0 - Added Nov 24, 2014

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.1.0-2015-02-10-09-36-51.zip)

## ver 1.9.0.1 - Added May 15, 2014

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.0.1-2015-02-10-09-40-57.zip)

## ver 1.9.0.0 - Added May 13, 2014

[Download Here](https://gitlab.com/magento1x/magento-community-edition-versions/raw/master/src/magento-1.9.0.0-2015-02-10-09-43-06.zip)
